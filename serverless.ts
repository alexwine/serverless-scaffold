import type { Serverless } from "serverless/aws";

const serverlessConfiguration: Serverless = {
  configValidationMode: "warn",
  service: {
    name: "option-deposit-emails",
    // app and org for use with dashboard.serverless.com
    // app: your-app-name,
    // org: your-org-name,
  },
  frameworkVersion: "2",
  custom: {
    webpack: {
      webpackConfig: "./webpack.config.js",
      includeModules: true,
    },
  },
  // Add the serverless-webpack plugin
  plugins: [
    "serverless-offline",
    "serverless-webpack",
    // "serverless-iam-roles-per-function",
    "serverless-prune-plugin",
  ],
  provider: {
    name: "aws",
    runtime: "nodejs12.x",
    apiGateway: {
      minimumCompressionSize: 1024,
    },
    iamRoleStatements: [
      {
        Effect: "Allow",
        Action: [
          "lambda:InvokeFunction",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes",
          "sqs:ReceiveMessage",
          "sqs:SendMessage",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
          "logs:CreateLogGroup",
          "lambda:CreateEventSourceMapping",
          "lambda:ListEventSourceMappings",
          "lambda:ListFunctions",
        ],
        Resource: "*",
      },
    ],
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: "1",
    },
  },
  functions: {
    hello: {
      handler: "handler.hello",
      events: [
        {
          http: {
            method: "get",
            path: "hello",
          },
        },
      ],
    },
    sample: {
      handler: "src/sample.hello",
      events: [
        {
          http: {
            method: "get",
            path: "wassup",
          },
        },
      ],
    },
  },
  // resources: {
  //   Resources: {
  //     optionDepositBaseQueue: {
  //       Type: "AWS::SQS::Queue",
  //       Properties: {
  //         QueueName: "optionDepositBaseQueue",
  //       },
  //     },
  //   },
  // },
};

module.exports = serverlessConfiguration;
